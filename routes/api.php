<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Post')->group(function () {
    Route::resource('posts', 'PostController')
        ->except(['create', 'edit']);

    Route::prefix('/posts/{post}/votes')->group(function () {
        Route::patch('/up', 'UpvoteController');
        Route::patch('/down', 'DownvoteController');
    });
});
