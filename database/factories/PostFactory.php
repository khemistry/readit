<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'link' => $faker->url,
        'details' => $faker->sentence(10),
        'votes' => $faker->numberBetween(-50, 200),
    ];
});
