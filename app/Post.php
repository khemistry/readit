<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'link',
        'details',
    ];

    public static function createFromRequest(array $data = [])
    {
        $post = new self();
        $post->fill($data);
        $post->votes = 0;
        $post->save();

        return $post;
    }

    public function updateFromRequest(array $data = [])
    {
        $this->fill($data);
        $this->save();

        return $this;
    }

    public function upvote()
    {
        $this->increment('votes');
    }

    public function downvote()
    {
        $this->decrement('votes');
    }
}
