<?php

namespace App\Http\Controllers\Post;

use App\Post;
use App\Http\Resources\Post as PostResource;
use App\Http\Controllers\Controller;

class DownvoteController extends Controller
{
    public function __invoke(Post $post)
    {
        $post->downvote();

        return new PostResource($post);
    }
}
