<?php

namespace App\Http\Controllers\Post;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Resources\Post as PostResource;
use App\Http\Resources\PostCollection as PostCollectionResource;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public function index()
    {
        return wrap(
            Post::orderBy('votes', 'desc')->get(),
            PostCollectionResource::class
        );
    }

    public function store(Request $request)
    {
        return wrap(
            Post::createFromRequest($request->all()),
            PostResource::class
        );
    }

    public function show(Post $post)
    {
        return wrap($post, PostResource::class);
    }

    public function update(Request $request, Post $post)
    {
        return wrap(
            $post->updateFromRequest($request->all()),
            PostResource::class
        );
    }

    public function destroy(Post $post)
    {
        $post->delete();

        return response('', 204);
    }
}
