<?php

namespace App\Http\Controllers\Post;

use App\Post;
use App\Http\Resources\Post as PostResource;
use App\Http\Controllers\Controller;

class UpvoteController extends Controller
{
    public function __invoke(Post $post)
    {
        $post->upvote();

        return new PostResource($post);
    }
}
