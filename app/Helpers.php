<?php

if (!function_exists('wrap')) {
    function wrap($item, $class)
    {
        return new $class($item);
    }
}
